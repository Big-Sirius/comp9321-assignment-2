# -*- coding: utf-8 -*-
# @Time    : 2019-11-21 20:18
# @Author  : Shuang Liang
# @File    : activityInterface.py
# @Software: PyCharm

import ActivityAdvice.cleanActivityDataset

# weather is an integer, 1 for raining, 0 for not raining
def suggestion(weather):
    result = {}
    df_activity = ActivityAdvice.cleanActivityDataset.cleanDataset()
    if weather == 1:
        df_result = df_activity[['title','location','xCal_description','cost','activityType','venueAddress','ageRange']][df_activity['rainy'] == 1]
    else:
        df_result = df_activity[['title', 'location', 'xCal_description', 'cost', 'activityType', 'venueAddress', 'ageRange']][df_activity['rainy'] == 0]
        # print(df_result.to_string())

    for index,row in df_result.iterrows():
        result[row['title']] = {}
        result[row['title']]['activity description'] = row['xCal_description']
        result[row['title']]['activityType'] = row['activityType']
        result[row['title']]['ageRange'] = row['ageRange']
        result[row['title']]['venueAddress'] = row['venueAddress']
        result[row['title']]['cost'] = row['cost']

    return result

if __name__ == '__main__':
    weather = 1
    result = suggestion(weather)
    for activity in result:
        print(activity,':')
        for i in result[activity]:
            print(i,': ',result[activity][i])

        print()



