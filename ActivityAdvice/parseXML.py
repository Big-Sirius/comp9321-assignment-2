# -*- coding: utf-8 -*-
# @Time    : 2019-11-18 22:26
# @Author  : Shuang Liang
# @File    : userInterface.py
# @Software: PyCharm

from xml.dom.minidom import parse
import csv


def readXML():
    # open xml
    domTree = parse("active-parks.rss")
    # open xml, coding utf-8
    file = open("parse.csv", "w", newline='', encoding="utf-8")
    # Set file write mode
    content = csv.writer(file, dialect='excel')
    # title line
    info = ['index', 'title', 'description', 'link', 'ealink', 'category', 'guid', 'masterid', 'summary', 'location', 'dtstart', 'localstart', 'formatteddatetime', 'dtend',
            'localend', 'cdo_alldayevent', 'xCal_description', 'uid', 'eventType1', 'cost', 'eventType2', 'bookings', 'activityType', 'eventImage', 'meetingPoint', 'age', 'venueAddress',
            'venue', 'ageRange', 'categorycalendar']
    # write title line
    content.writerow(info)
    # document root
    rootNode = domTree.documentElement
    # print(rootNode.nodeName)

    # all item label
    items = rootNode.getElementsByTagName("item")
    print("****all item label information ****", len(items))

    # Loop out the elements in the item
    for index, item in enumerate(items):
        info = []
        # print(index + 1, end='  ')
        info.append(str(index + 1))
        # Take out the label with the title
        info.append(item.getElementsByTagName("title")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("description")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("link")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("x-trumba:ealink")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("category")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("guid")[0].childNodes[0].data)
        # print(len(item.getElementsByTagName("x-trumba:masterid")))
        # x-trumba:masterid Label may not be available, need to judge
        if len(item.getElementsByTagName("x-trumba:masterid")) > 0:
            info.append(item.getElementsByTagName("x-trumba:masterid")[0].childNodes[0].data)
        else:
            info.append('')
        info.append(item.getElementsByTagName("xCal:summary")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("xCal:location")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("xCal:dtstart")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("x-trumba:localstart")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("x-trumba:formatteddatetime")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("xCal:dtend")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("x-trumba:localend")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("x-microsoft:cdo-alldayevent")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("xCal:description")[0].childNodes[0].data)
        info.append(item.getElementsByTagName("xCal:uid")[0].childNodes[0].data)

        # Remove the tag group named x-trumba:customfield
        customfields = item.getElementsByTagName("x-trumba:customfield")
        # Loop through, take different content based on id
        for customfield in customfields:
            if customfield.getAttribute("id") == '21':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '22177':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '21859':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '22732':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '23294':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '40':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '23560':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '23562':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '22505':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '22542':
                info.append(customfield.childNodes[0].data)
            if customfield.getAttribute("id") == '21858':
                info.append(customfield.childNodes[0].data)

        info.append(item.getElementsByTagName("x-trumba:categorycalendar")[0].childNodes[0].data)
        # write a line in csv
        content.writerow(info)
    # close file
    file.close()


if __name__ == '__main__':
    readXML()
