# -*- coding: utf-8 -*-
# @Time    : 2019-11-21 19:14
# @Author  : Shuang Liang
# @File    : cleanActivityDataset.py
# @Software: PyCharm
import pandas as pd

# get the dataset
def cleanDataset():
    df = pd.read_csv("ActivityAdvice/parse.csv")
    # select useful information
    df_activity = df[['index','title','location','xCal_description','cost','activityType', 'venueAddress','ageRange']]
    # set new index
    df_activity = df_activity.set_index('index')
    # deal with null
    df_activity['ageRange'] = df_activity['ageRange'].fillna('no limit')
    # create new column for weather
    df_activity['rainy'] = None

    # set the weather value

    for index, row in df_activity.iterrows():
        if ('Park' in row['location']) or ('Park' in row['title']) or ('river' in row['title']) or ('nature' in row['title']):
            row['rainy'] = 0
        else:
            row['rainy'] = 1

        if row['ageRange'] == "Brisbane's calendar|Active and healthy|Active parks":
            row['ageRange'] = 'no limit'
    return df_activity
