# -*- coding: utf-8 -*-
# @Time    : 2019-11-17 17:09
# @Author  : Shuang Liang
# @File    : users.py
# @Software: PyCharm

from pymongo import MongoClient

settings = {
    "ip":'182.92.195.97',                   #ip
    "port":27017,                           #port
    "db_name" : "SiriusWeatherSystem",      #database name
    "set_name" : "users"                    #collection name
}

class MyMongoDB(object):
    def __init__(self):
        try:
            self.conn = MongoClient(settings["ip"], settings["port"])
        except Exception as e:
            print(e)
        self.db = self.conn[settings["db_name"]]
        self.my_set = self.db[settings["set_name"]]

    #insert
    def insert(self,dic):
        self.my_set.insert(dic)

    #update
    def update(self,dic,newdic):
        self.my_set.update(dic,newdic)

    #delete
    def delete(self,dic):
        self.my_set.remove(dic)

    #query
    def dbFind(self,dic):
        data = self.my_set.find(dic)
        return data

    #query all
    def findAll(self):
        # 查询全部
        for i in self.my_set.find():
            print(i)

def user_query(mongo,name,password):

    dic_user = {"_id": name}
    find_user = mongo.dbFind(dic_user)
    if find_user.count() == 0:
        print("user does not exist")
        return False
    else:
        dic = {"_id": name, "password": password}
        data = mongo.dbFind(dic)
        if data.count() == 0:
            print("password is not correct")
            return False
        else:
            print("user and password are correct")
            for i in data:
                print(i)
            return True

def user_insert(mongo,name,password):
    dic_user = {"_id": name}
    find_user = mongo.dbFind(dic_user)
    if find_user.count() != 0:
        print("user name has already exist")
        return False
    else:
        dic = {"_id": name, "password": password}
        mongo.insert(dic)
        print("user insert successful")
        return True


def user_delete(mongo,name):
    dic_user = {"_id": name}
    find_user = mongo.dbFind(dic_user)
    if find_user.count() == 0:
        print("user does not exist")
        return False
    else:
        mongo.delete(dic_user)
        print("user delete successful")


def user_password_undate(mongo,name,password):
    dic_user = {"_id": name}
    find_user = mongo.dbFind(dic_user)
    if find_user.count() == 0:
        print("user does not exist")
        return False
    else:
        dic_password = {"$set": {"password": password}}
        mongo.update(dic_user,dic_password)
        print("update password successful")

if __name__ == "__main__":
    mongo = MyMongoDB()

    # query whether user exist and its password is correct
    user_query(mongo,"Jacinda","123232")

    # insert new user
    # user_insert(mongo,"Ivan","123232")

    # delete user
    # user_delete(mongo, "Jacinda")

    # update password
    # user_password_undate(mongo, "Ivan", "123")

    # mongo.findAll()





