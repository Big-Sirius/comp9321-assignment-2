import datetime

from flask import Flask, render_template, make_response, jsonify, request, Response
from flask_restplus import Api, Resource, fields
import json

import advice_Traffic
import query
import showRow
import token_create_verify
import userInterface
from ActivityAdvice import *
from ActivityAdvice import activityInterface
from Model import Model_Rainy, Model_MM

app = Flask(__name__)
api = Api(app,
          default="weather",
          title="weather Dataset",
          description="This is weather history search and future prediction")
s_total = 0
s_obtain_token = 0
s_register = 0
s_search_history_by_date_and_location = 0
s_get_api_statistic = 0
s_predict = 0

user_model = api.model('User', {
    'username': fields.String,
    'password': fields.String
})


@api.route('/s_obtain_token')
class ObtainToken(Resource):

    @api.response(200, 'username and password verified and token created')
    @api.response(401, 'Username or password incorrect')
    @api.doc(description="client obtain the token")
    def post(self):
        global s_total
        global s_obtain_token
        s_total += 1
        s_obtain_token += 1
        data = json.loads(request.get_data(as_text=True))
        username = data["username"]
        password = data["password"]
        if userInterface.user_query(username, password):
            return {"token": token_create_verify.create_token(username)}, 200
        else:
            api.abort(401, "Username or password incorrect")


@api.route('/s_register')
class Register(Resource):

    @api.response(200, 'Register successfully and token created')
    @api.response(409, 'Username duplicate')
    @api.doc(description="register for a new user")
    # @api.expect(user_model, validate=True)

    def post(self):
        global s_total
        s_total += 1
        global s_register
        s_register += 1
        data = json.loads(request.get_data(as_text=True))
        username = data["username"]
        password = data["password"]
        if userInterface.user_insert(username, password):
            return {"token": token_create_verify.create_token(username)}, 200
        else:
            api.abort(409, "Username duplicate")


@api.route('/s_search_history_by_date_and_location')
@api.param('location', 'The location u want to search')
@api.param('date', 'The date u want to search')
@api.param('start', 'The start index of u query data')
@api.param('length', 'The number of data u want to get')
class History(Resource):

    @api.response(200, 'Successful')
    @api.response(401, 'No token or token wrong')
    @api.doc(description="Get the filtered history data")
    def post(self):
        global s_total
        s_total += 1
        global s_search_history_by_date_and_location
        s_search_history_by_date_and_location += 1
        print(request.get_data(as_text=True))
        try:
            token = request.headers.get('token')
        except Exception:
            api.abort(401, 'No token or token wrong')
        if token_create_verify.verify_token(token) is None:
            api.abort(401, 'No token or token wrong')

        data = json.loads(request.get_data(as_text=True))
        str_time = ""
        if data["date"] != "":
            time = datetime.datetime.strptime(data["date"], '%Y-%m-%d').timetuple()
            str_time = str(time.tm_year) + '/' + str(time.tm_mon) + '/' + str(time.tm_mday)
        response_dic = query.weather_query(data["start"], data["length"], str_time,
                                           data["location"])
        print(response_dic)
        return {
                   "draw": data["draw"],
                   "recordsTotal": response_dic["total"],
                   "recordsFiltered": response_dic["total"],
                   "data": response_dic["data"]
               }, 200
        # data = json.loads(request.get_data(as_text=True))
        # print(data["draw"])


@api.route('/s_predict/<int:humidity>/<int:pressure>/<int:cloud>/<int:rain_today>/<int:rainfall>/<int:min_temp>')
@api.param('humidity', 'The humidity today')
@api.param('pressure', 'The pressure today')
@api.param('cloud', 'The cloud today')
@api.param('rain_today', 'Is it rain today')
@api.param('rainfall', 'The rainfall today')
@api.param('min_temp', 'The min temperature today')
class Predict(Resource):
    @api.response(200, 'predict the weather successfully')
    @api.doc(description="get the weather prediction")
    def get(self, humidity, pressure, cloud, rain_today, rainfall, min_temp):
        global s_predict
        global s_total
        s_predict += 1
        s_total += 1
        rain_tomorrow = Model_Rainy.predict([humidity, pressure, cloud, rain_today, rainfall])
        rainfall_tomorrow = Model_MM.predict([min_temp, humidity, cloud, rain_today])
        activity_dic = activityInterface.suggestion(rain_tomorrow)
        return_dic = {}
        i = 0;
        for key in activity_dic:
            return_dic[key] = [activity_dic[key]['activity description'], activity_dic[key]['activityType']]
        print(activity_dic)
        print(rain_tomorrow)
        print(return_dic)
        return {
                   "rain_tomorrow": rain_tomorrow,
                   "rain_mm": advice_Traffic.suggestion(rain_tomorrow),
                   "activity": return_dic
               }, 200
        # print(activityDic)


@api.route('/s_get_api_statistic/<string:api_name>')
@api.param('api_name', 'The name of the api')
class ApiStatistic(Resource):
    @api.response(200, 'get the using times of API successfully')
    @api.doc(description="get the using times of API")
    def get(self, api_name):
        global s_total
        global s_register
        global s_obtain_token
        global s_get_api_statistic
        global s_search_history_by_date_and_location
        s_total += 1
        s_get_api_statistic += 1
        call_times = 0
        if api_name == "all":
            return {"s_register": s_register, "s_obtain_token": s_obtain_token,
                    "s_get_api_statistic": s_get_api_statistic,
                    "s_search_history_by_date_and_location": s_search_history_by_date_and_location,
                    "s_predict": s_predict}, 200
        elif api_name == "s_register":
            call_times = s_register
        elif api_name == "s_obtain_token":
            call_times = s_obtain_token
        elif api_name == "s_get_api_statistic":
            call_times = s_get_api_statistic
        elif api_name == "s_search_history_by_date_and_location":
            call_times = s_search_history_by_date_and_location
        return {"api_name": api_name, "total_api_call_times": s_total, "api_call_times": call_times}, 200


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80)
