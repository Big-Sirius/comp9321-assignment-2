import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

df = pd.read_csv("weatherAUS.csv")
#print(df.head())
#print(df.tail())

df2 = df.drop(['Sunshine'],axis =1)
#print(df.head(5))
#print(df.tail(5))

df2 = df2.drop(['WindGustDir'],axis =1)
df2 = df2.drop(['WindDir9am'],axis =1)
df2 = df2.drop(['WindDir3pm'],axis =1)
#df = df.drop(['WindGustDir', 'WindDir9am','WindDir3pm'], axis = 0)
#print(df.head())
#print(df.tail())

df2 = df2.drop(['Rainfall'],axis =1)
#print(df1.head(5))
#print(df1.tail(5))

df2 = df2.drop(['Evaporation'],axis =1)
#print(df1.head(5))
#print(df1.tail(5))

df2 = df2.replace('No', 0)
df2 = df2.replace('Yes', 1)
#print(df1.head(10))
#print(df1.tail(5))

df2 = df2.dropna(axis=0,how='any')

import math
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error
import joblib

X = df2.iloc[:, [2,8,12,15]]
X.info()

Y = df2.RISK_MM
Y.head()

X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size=0.25)

from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error
linear = LinearRegression()
linear.fit(X_train, Y_train)
#print(cross_val_score(linear, X_train, Y_train, cv=5))

import joblib
joblib.dump(linear, "Weather_Au_Mm.ml")

import sys,json
import numpy as np
import joblib

def predict(data):
    data = np.array(data)
    sav = joblib.load('Weather_Au_Mm.ml')
    pred = sav.predict(data.reshape(1,-1))
    if 0<=pred <10:
        result = 0
    elif 10<=pred<15:
        result = 1
    else :
        result = 2
    return result

a = [40,100,8,1]

result = predict(a)

print(result)