import numpy as np
import pandas as pd

df = pd.read_csv("weatherAUS.csv")

df4 = df.drop(['Evaporation'], axis=1)
df4 = df4.drop(['Rainfall'], axis=1)
df4 = df4.drop(['WindGustDir'], axis=1)
df4 = df4.drop(['WindDir9am'], axis=1)
df4 = df4.drop(['WindDir3pm'], axis=1)
df4 = df4.drop(['Sunshine'], axis=1)
df4 = df4.drop(['WindGustSpeed'], axis=1)
df4 = df4.drop(['Temp9am'], axis=1)
df4 = df4.drop(['Temp3pm'], axis=1)
df4 = df4.drop(['WindSpeed3pm'], axis=1)
df4 = df4.drop(['Humidity3pm'], axis=1)
df4 = df4.drop(['Pressure3pm'], axis=1)
df4 = df4.drop(['Cloud3pm'], axis=1)

df4 = df4.replace(np.nan, "", regex=True)

'''x(y-1)+1, xy (x,y)   df[0:2]'''


def showrow(start, length):
    if (start + length) <= df.shape[0]:
        df5 = df4[start: (start + length)]
        'print(df4)'
        df_data = np.array(df5)
        df_data_list = df_data.tolist()
        '''print(df_data_list)'''

    else:
        df5 = df4[start: (df.shape[0])]
        'print(df4)'
        df_data = np.array(df5)
        df_data_list = df_data.tolist()
        '''print(df_data_list)'''

    return {"data": df_data_list, "total": df.shape[0]}


'''datarow = 50'''
'''pagenum = 1'''
'''a = showrow(50, 2)
print(a)
type(a)

df_data = np.array(a)
df_data_list = df_data.tolist()
print(df_data_list)

print(df.shape[0])'''


