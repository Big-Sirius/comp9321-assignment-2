import pandas as pd
import numpy as np

# Date from 2008-12-1 to 2017-6-24


def weather_query(start, length, date, location):
    df = pd.read_csv('weatherAUS.csv')
    df = df.replace(np.nan, "", regex=True)
    df_query = df[df['Location'].str.contains(location)]
    if date != "":
        df_query = df_query[df_query['Date'] == date]
    result = np.array(df_query[start:(start+length)]).tolist();
    return {"data": result, "total": df_query.shape[0]}


# print(weather_query('2008-12-01', 'Albury'))
# print(weather_query('2035-12-01', 'Albury')
if __name__ == '__main__':
    weather_query(0, 10, 1, "S")
