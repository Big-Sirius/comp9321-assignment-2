import pandas as pd
import numpy as np

pd.set_option('display.width', None)
pd.set_option('display.max_rows', 20)

df = pd.read_csv('Accident_Information.csv', low_memory=False)
df = df.drop(['Accident_Index'], axis=1)
df = df.drop(['1st_Road_Class'], axis=1)
df = df.drop(['1st_Road_Number'], axis=1)
df = df.drop(['2nd_Road_Class'], axis=1)
df = df.drop(['2nd_Road_Number'], axis=1)
df = df.drop(['Accident_Severity'], axis=1)
df = df.drop(['Carriageway_Hazards'], axis=1)
df = df.drop(['Day_of_Week'], axis=1)
df = df.drop(['Did_Police_Officer_Attend_Scene_of_Accident'], axis=1)
df = df.drop(['Junction_Control'], axis=1)
df = df.drop(['Junction_Detail'], axis=1)
df = df.drop(['Latitude'], axis=1)
df = df.drop(['Local_Authority_(District)'], axis=1)
df = df.drop(['Local_Authority_(Highway)'], axis=1)
df = df.drop(['Location_Easting_OSGR'], axis=1)
df = df.drop(['Location_Northing_OSGR'], axis=1)
df = df.drop(['Longitude'], axis=1)
df = df.drop(['LSOA_of_Accident_Location'], axis=1)
df = df.drop(['Number_of_Casualties'], axis=1)
df = df.drop(['Number_of_Vehicles'], axis=1)
df = df.drop(['Pedestrian_Crossing-Human_Control'], axis=1)
df = df.drop(['Pedestrian_Crossing-Physical_Facilities'], axis=1)
df = df.drop(['Police_Force'], axis=1)
df = df.drop(['Road_Type'], axis=1)
df = df.drop(['Special_Conditions_at_Site'], axis=1)
df = df.drop(['Speed_limit'], axis=1)
df = df.drop(['Urban_or_Rural_Area'], axis=1)
df = df.drop(['InScotland'], axis=1)
df = df.iloc[570011:]
# df = df.drop(['Year'], axis=1)
df1 = pd.read_csv('WeatherAUS.csv')
# df2 = pd.read_csv('sample.csv')
"""if df.iloc[0, 1] == df1.iloc[1, 0]:
    print('yes')
else:
    print(df.iloc[1, 0])
    print(df1.iloc[1, 0])
    print(df)"""
df3 = pd.merge(df, df1, on=['Date'])
print(df3)

