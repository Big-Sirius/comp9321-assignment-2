from flask import current_app
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

s = Serializer("SECRET_KEY", expires_in=3600)


def verify_token(token):
    try:
        data = s.loads(token)
        print(data["username"])
    except Exception:
        return None
    return data["username"]


def create_token(username):
    token = s.dumps({"username": username}).decode("utf-8")
    print(token)
    print(verify_token(token))

    return token
